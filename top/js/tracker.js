// var wt_Server = "http://127.0.0.1:8000/tracker/waypoint"; //localhost server
// var wt_Server = "http://179.223.120.8:8000/tracker/waypoint"; //localhost server
// var wt_Server = "https://198.199.81.21:8000/tracker/waypoint"; //localhost server
// var wt_Server = "http://192.168.0.7:8000/tracker/waypoint"; //localhost server

var wt_Server = "https://webcastx.net/webcast/tracker/waypoint"; //webcast server

var wt_xhr = new XMLHttpRequest();
var wt_UrlParams = {};
var wt_ResponseCookies = {};
var wt_PageParams = {};
var wt_Params = {};

var wt_Links = [];
var wt_Events = [];
var wt_Forms = [];
var wt_DomainCookies = [];

var leadtemp_vsl = 1;
var leadtemp_exitpopup = 2;
var leadtemp_form = 3;
var leadtemp_checkout = 5;
var leadtemp_printedbillet = 8;
var leadtemp_purchase = 10;
var leadtemp_email = 4;
var leadtemp_share = 3;

var wt_action_viewpage ='ViewPage';
var wt_action_exitpopup ='ExitPopup';
var wt_action_submitform ='SubmitForm';
var wt_action_initiatecheckout ='InitiateCheckout';
var wt_action_printedbillet ='PrintedBillet';
var wt_action_purchase ='Purchase';
var wt_action_reademail = 'ReadEmail';
var wt_action_share = 'Share';


var wt_id = 1;
var wt_name = 2;
var wt_class = 3;
var wt_tag = 4;

var wt_leadtemp = {vsl:leadtemp_vsl,
                   form:leadtemp_form,
                   exitpopup:leadtemp_exitpopup,
                   checkout:leadtemp_checkout,
                   email:leadtemp_email,
                   printedbillet:leadtemp_printedbillet,
                   purchase:leadtemp_purchase,
                   share:leadtemp_share
};

function wt_Assign(dict_1, dict_2){
    for (var key in dict_2){
        dict_1[key] = dict_2[key];
    }
}

function wt_EventListener(type, element, event, func){
    this.type = type;
    this.element = element;
    this.event = event;
    this.func = func;
}

function wt_ElementLink(type, element, link, param, val){
    this.type = type;
    this.element = element;
    this.link = link;
    this.param = param;
    this.val = val;
}

function wt_FormInput(type, element, param){
    this.type = type;
    this.element = element;
    this.param = param;
}

function wt_DomainCookie(link, param, val){
    this.link = link;
    this.param = param;
    this.val = val;
}

function wt_GetCookie(cookie_name) {
    var name = cookie_name + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function wt_SetCookie(cookie_name, cookie_value, expire_days) {
    var d = new Date();
    d.setTime(d.getTime() + (expire_days*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cookie_name + "=" + cookie_value + ";" + expires + ";path=/";
}

function wt_CookieIsSet(cookie_name) {
    var cookie_value = GetCookie(cookie_name);
    if (cookie_value != ""){ return true;}
    else{ return false;}

}

function wt_CreateFrame(iframe_id, iframe_url) {
    //<iframe id="<?php echo $iFrame; ?>" width="0" height="0" scrolling="no" style="opacity:0;"></iframe
    var iframe = document.createElement('iframe');
    iframe.setAttribute('id', iframe_id);
    iframe.setAttribute('src', iframe_url);

    iframe.style.width = '0';
    iframe.style.height = '0';
    iframe.style.scrolling = 'no';
    iframe.style = 'opacity:0';
    document.body.appendChild(iframe);
}

function wt_CreateMeta(httpEquiv, content, url){
    var meta = document.createElement('meta');
    if (url!=''){ 
        content = content + ';'+url;
    }
    meta.httpEquiv = httpEquiv;
    meta.content = content;
    document.body.appendChild(meta);
}

function wt_WindowLocation(delay,url){
    window.setTimeout(function() {
    window.location.href = url;
    }, delay);
}

function wt_GetDevice(){
    var device ='UK';

    //(function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) 
    //device = 'MB';})(navigator.userAgent||navigator.vendor||window.opera);

    if(navigator.userAgent.match(/Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|IEMobile/i)){
        device = 'MB';
    }
    else{device='PC';}

    return device;
}

//https://stackoverflow.com/a/1714899/212292
function wt_Serialize(obj) {
	var str = [];
	for(var p in obj)
		if (obj.hasOwnProperty(p)) {
			str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
		}
	return str.join("&");
}

function wt_Post(params){

    wt_xhr.open("POST", wt_Server, true);
    //Send the proper header information along with the request
    wt_xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
//     wt_xhr.setRequestHeader("Access-Control-Allow-Origin", "*");
//     wt_xhr.setRequestHeader("Access-Control-Allow-Headers", "*");
//     wt_xhr.setRequestHeader("Access-Control-Expose-Headers", "*");
//     wt_xhr.setRequestHeader("Access-Control-Allow-Credentials", "true");
    
    wt_xhr.send(wt_Serialize(params));
}

function wt_GetUrlParams() {
  var query = location.search.substr(1);
  if (query){
    query.split("&").forEach(function(part) {
      var item = part.split("=");
      wt_UrlParams[item[0]] = decodeURIComponent(item[1]);
    });
  }
}

function wt_GetResponseCookies(){
    var query = wt_xhr.getResponseHeader("Set-Cookie");
    if (query){
        query.split(",").forEach(function(part) {
            var cookie = part.split(";")[0].split('=');
            wt_ResponseCookies[cookie[0]] = decodeURIComponent(cookie[1]);
        });
    }
    
}

function wt_GetResponse(){
    var query = JSON.parse(wt_xhr.response);
    wt_ResponseCookies = query;
}

function wt_SetResponseCookies(){
    for (var key in wt_ResponseCookies){
        wt_SetCookie(key, wt_ResponseCookies[key],180);
    }
}

function wt_AddEvents(){
    for (var i =0; i < wt_Events.length; i++){
        if (wt_Events[i].type == wt_id){
            var element = document.getElementById(wt_Events[i].element)
            element.addEventListener(wt_Events[i].event,wt_Events[i].func);
        }
        else if (wt_Events[i].type == wt_name){
            var elements = document.getElementsByName(wt_Events[i].element)
            for (var j=0; j < elements.length; j++){
                elements[j].addEventListener(wt_Events[i].event,wt_Events[i].func);
            }
        }
        else if (wt_Events[i].type == wt_class){
            var elements = document.getElementsByClassName(wt_Events[i].element)
            for (var j=0; j < elements.length; j++){
                elements[j].addEventListener(wt_Events[i].event,wt_Events[i].func);
            }
        }
        else if (wt_Events[i].type == wt_tag){
            var elements = document.getElementsByTagName(wt_Events[i].element)
            for (var j=0; j < elements.length; j++){
                elements[j].addEventListener(wt_Events[i].event,wt_Events[i].func);
            }
        }
    }
}

function wt_UpdateLinks(){
    for (var i =0; i < wt_Links.length; i++){
        if (wt_Links[i].type == wt_id){
            var element = document.getElementById(wt_Links[i].element)
            wt_FillHRef(element,wt_Links[i].link, wt_Links[i].param, wt_Params[wt_Links[i].val]);
        }
        else if (wt_Links[i].type == wt_name){
            var elements = document.getElementsByName(wt_Links[i].element)
            for (var j=0; j < elements.length; j++){
                wt_FillHRef(elements[j],wt_Links[i].link, wt_Links[i].param, wt_Params[wt_Links[i].val]);
            }
        }
        else if (wt_Links[i].type == wt_class){
            var elements = document.getElementsByClassName(wt_Links[i].element)
            for (var j=0; j < elements.length; j++){
                wt_FillHRef(elements[j],wt_Links[i].link, wt_Links[i].param, wt_Params[wt_Links[i].val]);
            }
        }
        else if (wt_Links[i].type == wt_tag){
            var elements = document.getElementsByTagName(wt_Links[i].element)
            for (var j=0; j < elements.length; j++){
                wt_FillHRef(elements[j],wt_Links[i].link, wt_Links[i].param, wt_Params[wt_Links[i].val]);
            }
        }
    }
}

function wt_FillHRef(element, link, param, val){
    var href = element.href;
    
    if (link!=''){
        href = link;
    }
    
    var parts = href.split('?');
    
    if (parts.length>1){
        element.href = href + '&' + param + '=' + val;
    }
    else{
        element.href = href + '?' + param + '=' + val;
    }
}

function wt_EditLink(link, param, val){
    
    var parts = link.split('?');
    
    val = wt_Params[val]
    
    if (parts.length>1){
        link = link + '&' + param + '=' + val;
    }
    else{
        link = href + '?' + param + '=' + val;
    }
    
    return link
}


function wt_FillForms(){
    for (var i =0; i < wt_Forms.length; i++){
        if (wt_Forms[i].type == wt_id){
            var element = document.getElementById(wt_Forms[i].element)
            element.value = wt_Params[wt_Forms[i].param];
        }
        else if (wt_Forms[i].type == wt_name){
            var elements = document.getElementsByName(wt_Forms[i].element)
            for (var j=0; j < elements.length; j++){
                elements[j].value = wt_Params[wt_Forms[i].param];
            }
        }
        else if (wt_Forms[i].type == wt_class){
            var elements = document.getElementsByClassName(wt_Forms[i].element)
            for (var j=0; j < elements.length; j++){
                elements[j].value = wt_Params[wt_Forms[i].param];
            }
        }
        else if (wt_Forms[i].type == wt_tag){
            var elements = document.getElementsByTagName(wt_Forms[i].element)
            for (var j=0; j < elements.length; j++){
                elements[j].value = wt_Forms[i].param;
            }
        }
    }
}

function wt_SetDomainCookies(){
    for (var i =0; i < wt_DomainCookies.length; i++){
        var wt_iFrameId = 'wt_iFrame' + i.toString();
        
        var wt_iFrameUrl = wt_DomainCookies[i].link;
        
        if (wt_iFrameUrl.split('?').length >1){
            wt_iFrameUrl = wt_iFrameUrl +'&'+ wt_DomainCookies[i].param +'='+wt_Params[wt_DomainCookies[i].val];
        }
        else{
            wt_iFrameUrl = wt_iFrameUrl +'?'+ wt_DomainCookies[i].param +'='+wt_Params[wt_DomainCookies[i].val];
        }
        
        wt_CreateFrame(wt_iFrameId, wt_iFrameUrl);
    }
}

function wt_Retarget(){
    var wt_lpurl = wt_Params['wt_lpurl'];
    if (wt_lpurl!=''){
        wt_CreateMeta('refresh',2,wt_lpurl);
    }
}

function wt_InitTrack() {
    wt_GetUrlParams();
    wt_Assign(wt_Params, wt_PageParams);
    wt_Assign(wt_Params, wt_UrlParams);
    //Object.assign(wt_Params, wt_PageParams); //Object.assign nao eh suportado no IE.
    //Object.assign(wt_Params, wt_UrlParams); //Object.assign nao eh suportado no IE.
    
    var lead = wt_GetCookie('wt_lead');
    wt_Params['wt_lead_cookie'] = lead;
    
//     wt_Params['wt_lead'] = '8938f37719304b5994fdc2d3f8ce08ed';
    
    wt_xhr = new XMLHttpRequest();
    //wt_xhr.onload
    wt_xhr.onreadystatechange = function(){
        if (wt_xhr.readyState == 4 && wt_xhr.status == 200){
//         if (wt_xhr.readyState == 4){
            
            wt_GetResponse();
//             wt_GetResponseCookies();
            wt_SetResponseCookies();
            wt_Assign(wt_Params, wt_ResponseCookies);
            //Object.assign(wt_Params, wt_ResponseCookies);
            wt_UpdateLinks();
            wt_FillForms();
            wt_SetDomainCookies();
            wt_Retarget();
        }
    }; 
    wt_Post(wt_Params);
}

function wt_Checkout(){
    //alert('Checkout');
    var wt_CheckoutParams = {};
    
    wt_Assign(wt_CheckoutParams, wt_Params);        
    //Object.assign(wt_CheckoutParams, wt_Params);
    
    wt_CheckoutParams['wt_action'] = wt_action_initiatecheckout;
    wt_CheckoutParams['wt_leadtemp'] = leadtemp_checkout;
    
    wt_xhr = new XMLHttpRequest();
    wt_Post(wt_CheckoutParams);
}

function wt_SubmitForm(){
    //alert('SubmitForm');
    var wt_FormParams = {};
    
    wt_Assign(wt_FormParams, wt_Params);    
    //Object.assign(wt_FormParams, wt_Params);
    
    wt_FormParams['wt_action'] = wt_action_submitform;
    wt_FormParams['wt_leadtemp'] = leadtemp_form;
    
    wt_xhr = new XMLHttpRequest();
    wt_Post(wt_FormParams);
}

function wt_ExitPopup(){
    //alert('ExitPopup');
    var wt_ExitPopupParams = {};
        
    wt_Assign(wt_ExitPopupParams, wt_Params);
    //Object.assign(wt_ExitPopupParams, wt_Params);
    
    wt_ExitPopupParams['wt_action'] = wt_action_exitpopup;
    wt_ExitPopupParams['wt_leadtemp'] = leadtemp_exitpopup;
    
    wt_xhr = new XMLHttpRequest();
    wt_Post(wt_ExitPopupParams);
}


function wt_PrintedBillet(){
    //alert('Checkout');
    var wt_PrintedBilletParams = {};
    
    wt_Assign(wt_PrintedBilletParams, wt_Params);
    //Object.assign(wt_PrintedBilletParams, wt_Params);
    
    wt_PrintedBilletParams['wt_action'] = wt_action_printedbillet;
    wt_PrintedBilletParams['wt_leadtemp'] = leadtemp_checkout;  
    
    wt_xhr = new XMLHttpRequest();
    wt_Post(wt_PrintedBilletParams);
}

function wt_Purchase(){
    //alert('ExitPopup');
    var wt_PurchaseParams = {};
    
    wt_Assign(wt_PurchaseParams, wt_Params);
    //Object.assign(wt_PurchaseParams, wt_Params);
    
    wt_PurchaseParams['wt_action'] = wt_action_purchase;
    wt_PurchaseParams['wt_leadtemp'] = leadtemp_purchase; 
    
    wt_xhr = new XMLHttpRequest();
    wt_Post(wt_PurchaseParams);
}

function wt_Share(){
    //adicionar informacao se lead veio do compartilhamento no banco de dados
    var wt_ShareParams = {};
    
    wt_Assign(wt_ShareParams, wt_Params);
    //Object.assign(wt_ShareParams, wt_Params);
    
    wt_ShareParams['wt_action'] = wt_action_share;
    wt_ShareParams['wt_leadtemp'] = leadtemp_share;
    
    wt_xhr = new XMLHttpRequest();
    wt_Post(wt_ShareParams);
}

function wt_Whitepage(){
    wt_GetUrlParams();
    var val = wt_UrlParams['wt_lead'];
    wt_SetCookie('wt_lead',val,180);
}



function wt_Splice(id_form1, id_form2, fields_form1, fields_form2, master_form1, submit_form1, redirect_link, param, val){

    var form1 = document.getElementById(id_form1);
    var form2 = document.getElementById(id_form2);
    
    //Ler servidor para post dos formularios
    var form1_Server = form1.action;
    var form2_Server = form2.action;
       
    // Encontra todos os campos inputs dos formularios            
    var form1_inputs = form1.getElementsByTagName('input');
    var form2_inputs = form2.getElementsByTagName('input');
    
    //Captura inputs de interesse nos formularios
    var form1_fields = [];
    for (var i =0; i < fields_form1.length; i++){
        form1_fields.push(form1_inputs[fields_form1[i]]);
    }
    
    var form2_fields = [];
    for (var j =0; j < fields_form2.length; j++){
        form2_fields.push(form2_inputs[fields_form2[j]]);
    }

    
    //Atualiza dados do formulario secundario com dados do formulario master
    if(master_form1 == 1){
        for (var k =0; k < fields_form1.length; k++){
            form2_fields[k].value = form1_fields[k].value;
        }
    }
    else{
        for (var l =0; l < fields_form1.length; l++){
            form1_fields[l].value = form2_fields[l].value;
        }
    }
    
    if (redirect_link!=""){
        if (param!="" && val!="" ){
            redirect_link = wt_EditLink(redirect_link,param,val)
        }
        var form1_xhr = new XMLHttpRequest();
        form1_xhr.open("POST", form1_Server, true);
    
        form1_xhr.onreadystatechange = function(){ //Declara evento, que ao finalizar envio do form1, inicia envio do form2
            if (form1_xhr.readyState == 4){
                var form2_xhr = new XMLHttpRequest();
                form2_xhr.open("POST", form2_Server, true);
            
                form2_xhr.onreadystatechange = function(){ //Declara evento, que ao finalizar envio do form2, inicia redirecionamento
                    if (form2_xhr.readyState == 4){
                        //REDIRECIONAMENTO PARA LINK DESEJADO
                        //wt_CreateMeta('refresh',2,redirect_link);//Meta refresh nao funciona para FireFox (desabilitado por default)
                        wt_WindowLocation(2,redirect_link);
                    }
                };
                
                //Envia post para servidor do form2
                var form2_formData = new FormData( document.getElementById(id_form2) );
                form2_xhr.send(form2_formData);
            }
        };
        
        //Envia post para servidor do form1
        var form1_formData = new FormData( document.getElementById(id_form1) );
        form1_xhr.send(form1_formData);
    }
    else if(submit_form1==1){
        
        var form2_xhr = new XMLHttpRequest();
        form2_xhr.open("POST", form2_Server, true);
    
        form2_xhr.onreadystatechange = function(){ //Declara evento, que ao finalizar envio do form2, inicia envio do form1
            if (form2_xhr.readyState == 4){
                document.forms[id_form1].submit(); //Envia form1
            }
        };
        
        //Envia post para servidor do form2
        var form2_formData = new FormData( document.getElementById(id_form2) );
        form2_xhr.send(form2_formData);
        
    }
    else{
        var form1_xhr = new XMLHttpRequest();
        form1_xhr.open("POST", form1_Server, true);
    
        form1_xhr.onreadystatechange = function(){ //Declara evento, que ao finalizar envio do form1, inicia envio do form2
            if (form1_xhr.readyState == 4){
                document.forms[id_form2].submit(); //Envia form2
            }
        };
        
        //Envia post para servidor do form1
        var form1_formData = new FormData( document.getElementById(id_form1) );
        form1_xhr.send(form1_formData);
    }
}


function wt_GetCookiesAndURLParams(){
    wt_GetUrlParams();
    var wt_lead = wt_GetCookie('wt_lead');
    var src = wt_GetCookie('src');     
    wt_Params['wt_lead'] = wt_lead;
    wt_Params['src'] = src;
    
    wt_Assign(wt_Params, wt_UrlParams);
}
        
    
        

